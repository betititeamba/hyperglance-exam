package com.hyperglance;

import java.util.List;

public class CountingUtil {

    public static int countLessThan(List<Integer> numbers, int threshold) {
        int lower = numbers.size();
        int upper = numbers.size();

        while (numbers.get(lower - 1) >= threshold) {
            upper = lower;
            lower = (lower / 2) + (lower % 2);

            if (lower == 1 && numbers.get(lower - 1) >= threshold) {
                lower = 0;
                break;
            }
        }

        return lower > 0
                ? lower + getRemaining(numbers, threshold, lower, upper)
                : 0;
    }

    private static int getRemaining(List<Integer> numbers, int threshold, int lower, int upper) {
        return (int) numbers.subList(lower, upper).stream().filter(i -> i < threshold).count();
    }
}
