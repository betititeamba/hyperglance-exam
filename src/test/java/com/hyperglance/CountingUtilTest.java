package com.hyperglance;

import org.junit.Test;

import java.util.List;

import static com.hyperglance.CountingUtil.countLessThan;
import static org.junit.Assert.assertEquals;

public class CountingUtilTest {

    @Test
    public void test() {
        assertEquals(4, countLessThan(List.of(1, 3, 3, 4, 8), 5));
        assertEquals(2, countLessThan(List.of(7, 8, 9, 9, 10), 9));
        assertEquals(3, countLessThan(List.of(1, 5, 6, 7, 10), 7));
        assertEquals(4, countLessThan(List.of(1, 5, 6, 7, 8), 8));
        assertEquals(5, countLessThan(List.of(1, 5, 6, 7, 8), 9));
        assertEquals(1, countLessThan(List.of(1, 4, 6, 7, 8), 4));
        assertEquals(0, countLessThan(List.of(3, 4, 6, 7, 8), 3));
        assertEquals(0, countLessThan(List.of(3), 3));
        assertEquals(1, countLessThan(List.of(2), 3));
    }
}
